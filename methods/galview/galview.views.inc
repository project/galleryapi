<?php

/**
 * @file
 * Theming and formatting functions for Views Output Method.
 */

function template_preprocess_galview_view_galview(&$vars) {
  $preset = galleryapi_preset_load($vars['view']->style_options['presets']);

  foreach ($vars['view']->result as $node) {
    $data['node'. $node->nid] = TRUE;  
  }
  
  $vars['gallery'] = galleryapi_build_gallery($preset['pid'], $data);
}