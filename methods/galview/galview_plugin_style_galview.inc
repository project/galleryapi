<?php

/**
 * @file
 * Views integration functions for Views Output Method.
 */

class galview_plugin_style_galview extends views_plugin_style {

  function option_definition() {
    $options = parent::option_definition();
    
    $options['presets'] = array('default' => 0); 
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $options[0] = t('--Select preset--');
    foreach (galleryapi_preset_load() as $preset) {
      if ($preset['input_name'] == 'view') {
        $options[$preset['pid']] = $preset['name'];
      }
    }
    if (count($options) === 1) {
      $options[0] = t('There are no avialable presets found!');
    }
    $form['presets'] = array(
      '#type' => 'select',
      '#title' => t('Gallery API Preset'),
      '#options' => $options,
      '#default_value' => $this->options['presets'],
    );
    
  }
}