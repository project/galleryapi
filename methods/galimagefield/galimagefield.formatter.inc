<?php

/**
 * @file
 * Theming and formatting functions for Image Field Input Method.
 */

function theme_galimagefield($element) {
  $output = '';
  static $gallery_cache;
  
  foreach (galleryapi_preset_load() as $preset) {
    if ('galleryapi_cckimagefield_'. $preset['name'] === $element['#formatter']) {
      $pid = $preset['pid'];
      break;
    }
  }
  
  $gallery_id = 'node-'. $element['#node']->nid .'-'. $element['#field_name'];
  if ($pid && is_numeric($pid)) {
    if (!isset($gallery_cache[$gallery_id])) {
      $data = array();
      foreach ($element as $key => $item) {
        if (is_numeric($key) && $item['#item']['filename']) {
          $slide_id = $gallery_id .'-'. $key;
          $data[$slide_id] = TRUE;        
        }
      } 
      
      $gallery_cache[$gallery_id] = galleryapi_build_gallery($pid, $data);
      $output .= $gallery_cache[$gallery_id];
    }
    else {
      $output .= $gallery_cache[$gallery_id];
    }    
  }

  return $output;
}