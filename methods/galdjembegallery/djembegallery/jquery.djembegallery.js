
(function(jQuery) {

var djembeGallery;
var djembeGalleryId = 1;

djembeGallery = jQuery.fn.djembeGallery = function(options) {

  var options = jQuery.extend({
    navigationLinks: 'bottom', // 'top', 'middle' or 'bottom'
    navigationLinksType: 'text', // 'text' or 'image'
    navigationFirstLink: '<< first', // '' or any text
    navigationPrevLink: '< previous', // '' or any text
    navigationNextLink: 'next >', // '' or any text
    navigationLastLink: 'last >>', // '' or any text
    navigationSlideshowStartLink: 'play', // '' or any text
    navigationSlideshowStopLink: 'stop', // '' or any text
    
    defaultItem: true, // true, false or itemId
    
    slideshow: true, // true or false 
    slideshowDelay: 1, // time in seconds
    slideshowAutoStart: false, // true or false
    slideshowSequence: 'forward', // 'forward', 'backward' or 'random'
    
    itemViewExt: 'png', // '' or file extension
    itemViewUrl: function(itemId) {}, // path or function
    itemViewEvent: 'click', // event
    itemViewTransition: function(item_selector, old_item_selector, options) {
      var itemObj = jQuery(item_selector).fadeIn(options.itemViewTransitionTime*1000);
      jQuery(old_item_selector).fadeOut(options.itemViewTransitionTime*1000);
      
      if(!options._viewBoxCSSHeight) {
    	jQuery(options.viewBox).animate({height: itemObj.height()}, options.itemViewTransitionTime*1000);  
      }  

      return true;
    },
    itemViewTransitionTime: 0.5, // time in seconds    
    
    onPreviewMouseOver: function() {}, // function
    onPreviewMouseOut: function() {}, // function
    
    _viewBoxCSSHeight: false
  },options);

  
  return this.each(function() {
    options = jQuery.extend({
      gallery: this,
      gallerySelector: function() {
        var gal = jQuery(this.gallery); 
        if(!gal.attr('id')) {
          gal.attr('id', 'djembeGallery-'+djembeGalleryId);
          djembeGalleryId++;
        }
        return '#'+gal.attr('id');
      },
      previewBox: function() {
        return (this.gallerySelector ? this.gallerySelector+' ' : '') + '.preview-box';
      },
      viewBox: function() {
        var viewBox = (this.gallerySelector ? this.gallerySelector+' ' : '') + '.view-box';
        
        var viewBoxobj = jQuery(viewBox); 
        if(!viewBoxobj.is(viewBox)) {
          jQuery('<div class="view-box"></div>').prependTo(this.gallerySelector);
        }
        
        if(viewBoxobj.height()) {
          this._viewBoxCSSHeight = true;
        }
        return viewBox;
      },
      navigationBox: function() {
        return (this.gallerySelector ? this.gallerySelector+' ' : '') + '.navigation-box';
      }
    },options);

    if(jQuery.isFunction(options.gallerySelector)) {
      options.gallerySelector = options.gallerySelector();
    }
    if(jQuery.isFunction(options.previewBox)) {
      options.previewBox = options.previewBox();
    }
    if(jQuery.isFunction(options.viewBox)) {
      options.viewBox = options.viewBox();
    }
    if(jQuery.isFunction(options.navigationBox)) {
      options.navigationBox = options.navigationBox();
    }

    this.djembeOptions = options;
    
    jQuery(options.previewBox).children()
      .bind(options.itemViewEvent,
            function() {
              var itemId = this.id.split('-djembe-preview')[0];
                djembeGallery.activate(options, itemId);
            })
      .addClass('slide-preview')
      .hover(options.onPreviewMouseOver, options.onPreviewMouseOut);
    
    if(options.navigationLinks && !jQuery(options.navigationBox).is(options.navigationBox)) {
      navigationLinks = options.navigationLinksType == 'image' ? 
        jQuery('<div class="navigation-box navigation-box-image"></div>') :
        jQuery('<div class="navigation-box navigation-box-text"></div>');
      
      navigationLinks.hide();
      
      if(options.navigationFirstLink) {
        button = options.navigationLinksType == 'image' ? 
          jQuery('<a class="first image-first" href="#first"></a>') : 
          jQuery('<a class="first" href="#first">'+options.navigationFirstLink+'</a>');
        
        button
          .click(function() {
                  djembeGallery.first(options);
                  return false;
                 })
          .appendTo(navigationLinks);
      }
      if(options.navigationPrevLink) {
        button = options.navigationLinksType == 'image' ? 
          jQuery('<a class="prev image-prev" href="#prev"></a>') : 
          jQuery('<a class="prev" href="#prev">'+options.navigationPrevLink+'</a>');
        
        button
          .click(function() {
                  djembeGallery.prev(options);
                  return false;
                 })
          .appendTo(navigationLinks);
      }
      if(options.slideshow && options.navigationSlideshowStartLink) {
        button = options.navigationLinksType == 'image' ? 
          jQuery('<a class="play image-play" href="#play"></a>') : 
          jQuery('<a class="play" href="#play">'+options.navigationSlideshowStartLink+'</a>');
        
        button
          .click(function() {
                  djembeGallery.play(options);
                  return false;
                 })
          .appendTo(navigationLinks);
      }
      if(options.slideshow && options.navigationSlideshowStopLink) {
        button = options.navigationLinksType == 'image' ? 
          jQuery('<a class="stop image-stop" href="#stop"></a>') : 
          jQuery('<a class="stop" href="#stop">'+options.navigationSlideshowStopLink+'</a>');
        
        button
          .click(function() {
                  djembeGallery.stop(options);
                  return false;
                 })
          .hide()
          .appendTo(navigationLinks);
      }
      if(options.navigationNextLink) {
        button = options.navigationLinksType == 'image' ? 
          jQuery('<a class="next image-next" href="#next"></a>') : 
          jQuery('<a class="next" href="#next">'+options.navigationNextLink+'</a>');
        
        button
          .click(function() {
                  djembeGallery.next(options);
                  return false;
                 })
          .appendTo(navigationLinks);
      }
      if(options.navigationLastLink) {
        button = options.navigationLinksType == 'image' ? 
          jQuery('<a class="last image-last" href="#last"></a>') : 
          jQuery('<a class="last" href="#last">'+options.navigationLastLink+'</a>');
        
        button
          .click(function() {
                  djembeGallery.last(options);
                  return false;
                 })
          .appendTo(navigationLinks);
      }
      
      if(options.navigationLinks == 'top') {
        navigationLinks.prependTo(jQuery(options.gallery)).addClass('navigation-box-top').show();
      }
      else if(options.navigationLinks == 'bottom') {
        navigationLinks.appendTo(jQuery(options.gallery)).addClass('navigation-box-bottom').show();
      }
      else if(options.navigationLinks == 'middle') {
        jQuery(options.previewBox).before(navigationLinks);
        navigationLinks.addClass('navigation-box-middle').show();
      }
    }
    
    // Loader
    var viewBox = jQuery(options.viewBox);
    options.loaderBoxObj = jQuery('<div class="loader-box"></div>')
                             .css({'position': 'absolute'})
                             .hide();
    viewBox.after(options.loaderBoxObj);

    // default Item
    if(options.defaultItem === true) {
      djembeGallery.first(options);
    }
    else{
      obj = jQuery(options.previewBox+' #'+options.defaultItem+'-djembe-preview');
      if(obj.is('#'+options.defaultItem+'-djembe-preview')) {
        djembeGallery.activate(options, options.defaultItem);
      }
      else {
        djembeGallery.first(options);
      }
    }
    
    // Slideshow auto play
    if(options.slideshow && options.slideshowAutoStart) {
      djembeGallery.play(options);
    }
  });
};

djembeGallery.activate = function(options, itemId) {
  var obj = jQuery(options.previewBox+' #'+itemId+'-djembe-preview');
	  
  if(obj.is('#'+itemId+'-djembe-preview')) {
    djembeGallery._preloadSlide(options, itemId);
    djembeGallery._showSlide(options, itemId);
  }
}

djembeGallery._preloadSlide = function(options, itemId) {
 // alert(itemId);
  
  var obj = jQuery(options.viewBox+' #'+itemId+'-djembe-view');
  if(!obj.is('#'+itemId+'-djembe-view')) {
	  
	var obj2 = jQuery('#'+itemId+'-djembe-view');    
    if(obj2.is('#'+itemId+'-djembe-view')) {
      // TODO: Sames ID's :( 
      jQuery('<div id="'+itemId+'-djembe-view">').hide().appendTo(jQuery(options.viewBox)).html(obj2.html());
    }
    else {
      var viewBox = jQuery(options.viewBox);
      options.loaderBoxObj
        .css({'height': viewBox.height(),
              'width': viewBox.width(),
              'background-color': 'white',
              'opacity': 0.7,
              'margin-top': -1*viewBox.height()
             })
        .show();
    
      var url = '';
      if(jQuery.isFunction(options.itemViewUrl)) {
        url = options.itemViewUrl(itemId);
      }
      else {
    	if (options.itemViewUrl) {
    	  url = (options.itemViewUrl[options.itemViewUrl.length-1] == '/' ? options.itemViewUrl : options.itemViewUrl + '/');
    		
    	}    	  
        url += itemId + (options.itemViewExt ? '.'+options.itemViewExt : '');
      }
          
      if((url.indexOf('.png') == url.length - 4) ||
         (url.indexOf('.jpg') == url.length - 4) ||
         (url.indexOf('.jpeg') == url.length - 5) ||
         (url.indexOf('.gif') == url.length - 4)) {
        
        var image = new Image();
        image.onload = function() {
          jQuery('<div id="'+itemId+'-djembe-view">').appendTo(jQuery(options.viewBox)).hide().html('<img src="'+this.src+'" />');
          options.loaderBoxObj.hide();
          djembeGallery._showSlide(options, itemId);
          //jQuery(options.viewBox).html(jQuery('#'+itemId+'-djembe-view').html());          
          }
        image.src = url;
        }
      else {
        
        jQuery.ajax({
          url: url,
          data: {item: itemId},
          dataType: 'html',
          beforeSend: function() {
          },
          success: function(ret){
            jQuery('<div id="'+itemId+'-djembe-view">').appendTo(jQuery(options.viewBox)).hide().html(ret);            
            //options.loaderBoxObj.fadeOut(options.itemViewTransition*1000);
            options.loaderBoxObj.hide();
            djembeGallery._showSlide(options, itemId);
            //jQuery(options.viewBox).html(jQuery('#'+itemId+'-djembe-view').html());
          },
          error: function() {
        	options.loaderBoxObj.hide();
          }
        });
      }
    }
    //alert('need');
  }
}

djembeGallery._showSlide = function(options, itemId) {

  var obj = jQuery(options.viewBox+' #'+itemId+'-djembe-view');
  if(obj.is('#'+itemId+'-djembe-view')) {
	obj.css({'position': 'absolute'});
	
	var oldSlideId = '';
	oldSlideObj = jQuery(options.previewBox+' > .active');
	if(oldSlideObj.is(options.previewBox+' > .active')) {
	  oldSlideId = jQuery(options.previewBox+' > .active').attr('id').split('-djembe-preview')[0];
	}
	
	if(oldSlideId != itemId) {
      if(options.itemViewTransition(options.viewBox+' #'+itemId+'-djembe-view', options.viewBox+' #'+oldSlideId+'-djembe-view', options)) {
        jQuery(options.previewBox+' #'+oldSlideId+'-djembe-preview').removeClass('active');
        jQuery(options.previewBox+' #'+itemId+'-djembe-preview').addClass('active');
      }
	}
  }
}

djembeGallery.random = function(options) {
  var childrens = jQuery(options.previewBox).children();

  var oldSlideId = '';
  oldSlideObj = jQuery(options.previewBox+' > .active');
  if(oldSlideObj.is(options.previewBox+' > .active')) {
    oldSlideId = jQuery(options.previewBox+' > .active').attr('id').split('-djembe-preview')[0];
  }
  
  var stopFlag = 11;
  do {
	itemId = jQuery(childrens.get(Math.floor(childrens.size()*Math.random()))).attr('id').split('-djembe-preview')[0];	  
	stopFlag--;
  } while((oldSlideId == itemId) && stopFlag);
  
  djembeGallery.activate(options, itemId);
}

djembeGallery.next = function(options) {
  if(jQuery(options.previewBox+' > .active').is(options.previewBox+' > .active')) {
    var itemId = jQuery(options.previewBox+' > .active').is(':last-child') ?
                    jQuery(options.previewBox+' > .active').siblings(':first-child').attr('id').split('-djembe-preview')[0] :
                    jQuery(options.previewBox+' > .active').next().attr('id').split('-djembe-preview')[0];
    djembeGallery.activate(options, itemId);
  }
  else {
    var itemId = jQuery(options.previewBox+' > *:first').attr('id').split('-djembe-preview')[0];
    djembeGallery.activate(options, itemId);
  }   
}

djembeGallery.prev = function(options) {
  if(jQuery(options.previewBox+' > .active').is(options.previewBox+' > .active')) {
    var itemId = jQuery(options.previewBox+' > .active').is(':first-child') ?
                    jQuery(options.previewBox+' > .active').siblings(':last-child').attr('id').split('-djembe-preview')[0] :
                    jQuery(options.previewBox+' > .active').prev().attr('id').split('-djembe-preview')[0];
    djembeGallery.activate(options, itemId);
  }
  else {
    var itemId = jQuery(options.previewBox+' > *:last').attr('id').split('-djembe-preview')[0];
    djembeGallery.activate(options, itemId);
  }
}

djembeGallery.first = function(options) {
  var itemId = jQuery(options.previewBox+' > *:first').attr('id').split('-djembe-preview')[0];
  djembeGallery.activate(options, itemId);
}

djembeGallery.last = function(options) {
  var itemId = jQuery(options.previewBox+' > *:last').attr('id').split('-djembe-preview')[0];
  djembeGallery.activate(options, itemId);
}

djembeGallery.play = function(options) {
  options.slideshowTimer = setTimeout(function () {djembeGallery.slideshowNext(options)}, options.slideshowDelay*1000);
  jQuery(options.navigationBox+' .play').hide();
  jQuery(options.navigationBox+' .stop').show();
}

djembeGallery.stop = function(options) {
  clearTimeout(options.slideshowTimer);
  options.slideshowTimer = 0;
  jQuery(options.navigationBox+' .stop').hide();
  jQuery(options.navigationBox+' .play').show();
}

djembeGallery.slideshowNext = function(options) {	
  if(options.slideshowSequence == 'forward') {
    djembeGallery.next(options);
  }
  else if(options.slideshowSequence == 'backward') {
    djembeGallery.prev(options);
  }
  else if(options.slideshowSequence == 'random') {
    djembeGallery.random(options);
  }

  if(options.slideshowTimer) {
    options.slideshowTimer = setTimeout(function () {djembeGallery.slideshowNext(options)}, options.slideshowDelay*1000);
  }
}


jQuery.fn.djembeActivate = function(itemId) {  
  return this.each(function() {
    if(this.djembeOptions && itemId) {
      djembeGallery.activate(this.djembeOptions, itemId);
    }
  });
};

jQuery.fn.djembeRandom = function() {
  return this.each(function() {
    if(this.djembeOptions) {
      djembeGallery.random(this.djembeOptions);
    }
  });
};

jQuery.fn.djembeNext = function() {
  return this.each(function() {
    if(this.djembeOptions) {
      djembeGallery.next(this.djembeOptions);
    }
  });
};

jQuery.fn.djembePrev = function() {
  return this.each(function() {
    if(this.djembeOptions) {
      djembeGallery.prev(this.djembeOptions);
    }
  });
};

jQuery.fn.djembeFirst = function() {
  return this.each(function() {
    if(this.djembeOptions) {
      djembeGallery.first(this.djembeOptions);  
    }
  });
};

jQuery.fn.djembeLast = function() {
  return this.each(function() {
    if(this.djembeOptions) {
      djembeGallery.last(this.djembeOptions);
    }
  });
};

jQuery.fn.djembePlay = function() {
  return this.each(function() {
    if(this.djembeOptions) {
      djembeGallery.play(this.djembeOptions);
    }
  });
};

jQuery.fn.djembeStop = function() {
  return this.each(function() {
    if(this.djembeOptions) {
      djembeGallery.stop(this.djembeOptions);
    }
  });
};

})(jQuery);