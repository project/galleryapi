<?php

/**
 * @file
 * Provides administrative functions for galdjembegallery module.
 */ 

function galdjembegallery_galleryapi_djembegallery_form() {
  drupal_add_css(drupal_get_path('module', 'galdjembegallery') .'/galdjembegallery.admin.css'); 

  $form['navigation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Navigation box'),
    '#collapsible' => TRUE,
  );
  
  $form['navigation']['navigation_vis'] = array(
    '#type' => 'select',
    '#title' => t('Visibility'),
    '#options' => array('none' => t('None (default)'), 'top' => t('Top'), 'middle' => t('Middle'), 'bottom' => t('Bottom')),
  );
  
  $form['navigation']['navigation_type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => array('text' => t('Text'), 'image' => t('Image')),  
  );
  
  $form['navigation']['buttons'] = array(
    '#type' => 'fieldset',
    '#title' => t('Buttons'),
  );
  
  $form['navigation']['buttons']['first_vis'] = array(
    '#type' => 'checkbox',
    '#default_value' => 1, 
  );
  $form['navigation']['buttons']['first_text'] = array(
    '#type' => 'textfield',
    '#title' => t('First'),
    '#default_value' => t('<< first'),    
  );
  
  $form['navigation']['buttons']['prev_vis'] = array(
    '#type' => 'checkbox',
    '#default_value' => 1,
    '#prefix' => '<br /><br />',
  );
  $form['navigation']['buttons']['prev_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Previous'),
    '#default_value' => t('< previous'),    
  );
  
  $form['navigation']['buttons']['next_vis'] = array(
    '#type' => 'checkbox',
    '#default_value' => 1,  
    '#prefix' => '<br /><br />',  
  );
  $form['navigation']['buttons']['next_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Next'),
    '#default_value' => t('next >'),    
  );
  
  $form['navigation']['buttons']['last_vis'] = array(
    '#type' => 'checkbox',
    '#default_value' => 1,
    '#prefix' => '<br /><br />',    
  );
  $form['navigation']['buttons']['last_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Last'),
    '#default_value' => t('last >>'),    
  );
  
  $form['navigation']['buttons']['playstop_vis'] = array(
    '#type' => 'checkbox',
    '#default_value' => 1,
    '#prefix' => '<br /><br />',    
  );
  $form['navigation']['buttons']['play_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Play'),
    '#default_value' => t('play'),    
  );
  $form['navigation']['buttons']['stop_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Stop'),
    '#default_value' => t('stop'),
    '#prefix' => '<br />',    
  );
  
  $form['preview_box'] = array(
    '#type' => 'fieldset',
    '#title' => t('Preview box'),
    '#collapsible' => TRUE,
  );
  $form['preview_box']['preview_box_vis'] = array(
    '#type' => 'checkbox',
    '#default_value' => 1,    
    '#title' => t('Visibility'),
  );
  $form['preview_box']['preview_box_process_event'] = array(
    '#type' => 'select',
    '#title' => t('Full slide invocation event'),
    '#options' => array(
      'click' => t('Click'),
      'mouseover' => t('Hover'),
      'dblclick' => t('Double click'),      
    ),
  );
  
  $form['view_box'] = array(
    '#type' => 'fieldset',
    '#title' => t('View box'),
    '#collapsible' => TRUE,
  );
  $form['view_box']['view_box_delayed_load'] = array(
    '#type' => 'checkbox',
    '#default_value' => 1,    
    '#title' => t('Full slides lazy load (ajax)'),
  );
  $form['view_box']['view_box_transition_effect'] = array(
    '#type' => 'select',
    '#title' => t('Transition effect'),
    '#options' => array(
      'fade' => t('Fade'),
      'slide' => t('Slide'),
      'zoom' => t('Zoom'),
    ),
  );
  $form['view_box']['view_box_transition_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Transition speed'),
    '#default_value' => 0.5,
    '#description' => t('Speed of the transition effect in seconds.'),
  );
  
  $form['slideshow'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slideshow'),
    '#collapsible' => TRUE,
  );
  $form['slideshow']['slideshow_autoplay'] = array(
    '#type' => 'checkbox',
    '#default_value' => 0,
    '#title' => t('Slideshow autoplay'),    
  );
  $form['slideshow']['slideshow_sequence'] = array(
    '#type' => 'select',
    '#title' => t('Visibility'),
    '#options' => array('forward' => t('Forward'), 'backward' => t('Backward'), 'random' => t('Random')),
  );
  $form['slideshow']['slideshow_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Slideshow delay'),
    '#default_value' => 5,
    '#description' => t('Delay in seconds.'),    
  );
  
  return $form; 
}
