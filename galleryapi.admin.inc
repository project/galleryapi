<?php

/**
 * @file
 * Administrative functions for Gallery API.
 */ 

function galleryapi_admin_settings_callback() {
  $output = '';
  
  $output .= theme('galleryapi_preset_list', galleryapi_preset_load());
  $output .= drupal_get_form('galleryapi_admin_preset_edit');
  
  return $output;
}

function galleryapi_admin_preset_edit($form_state, $preset = array()) {
  $form_state['pid'] = $preset['pid'];

  $form = array();
  $form['create_presets'] = array(
    '#type' => 'fieldset',
    '#title' => t('Create gallery preset'),
    '#collapsible' => TRUE,    
  );
  $form['create_presets']['name'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#title' => t('Name'),
    '#required' => TRUE,
    '#default_value' => $preset['name']
  );
  $form['create_presets']['input'] = array(
    '#type' => 'select',
    '#title' => t('Input method'),
    '#options' => galleryapi_get_input_names(),
    '#required' => TRUE,
    '#default_value' => $preset['input_name'],
  );
  $form['create_presets']['output'] = array(
    '#type' => 'select',
    '#title' => t('Output method'),
    '#options' => galleryapi_get_output_names(),
    '#required' => TRUE,
    '#default_value' => $preset['output_name'],
  );
  $form['create_presets']['submit'] = array(
    '#type' => 'submit',
    '#value' => $preset['pid'] ? t('Save') : t('Create'),
  );
  
  return $form;
}

function galleryapi_admin_preset_edit_submit($form, &$form_state) {
  if (!$form_state['pid']) {
    db_query("INSERT INTO {galleryapi_presets} (pid, name, input_name, output_name) VALUES (%d, '%s', '%s', '%s')", '', $form_state['values']['name'], $form_state['values']['input'], $form_state['values']['output']);
    $pid = db_last_insert_id('galleryapi_presets', 'pid');    

    // hook_galleryapi_methodapi (operation: preset insert)      
    module_invoke_all('galleryapi_methodapi', 'preset insert', $pid, $form_state['value']);    
  }
  else {
    $pid = $form_state['pid'];
    db_query("UPDATE {galleryapi_presets} SET name = '%s', input_name = '%s', output_name = '%s' WHERE pid = %d", $form_state['values']['name'], $form_state['values']['input'], $form_state['values']['output'], $pid);
  
    // hook_galleryapi_methodapi (operation: preset save)      
    module_invoke_all('galleryapi_methodapi', 'preset save', $pid, $form_state['value']);
  }
  
  $form_state['redirect'] = 'admin/settings/galleryapi/'. $pid .'/configure';
}

function galleryapi_admin_preset_configure($form_state, $preset) {
  $form_state['pid'] = $preset['pid'];

  $input_method = galleryapi_get_input($preset['input_name']);
  $output_method = galleryapi_get_output($preset['output_name']);
  
  $form = array();
  $form['#tree'] = TRUE;
  $form['input'] = array(
    '#type' => 'fieldset',
    '#title' => t('Input method %input_name settings', array('%input_name' => $input_method['name'])),
  );

  if ($input_method['admin_file']) {
    require_once drupal_get_path('module', $input_method['module']) .'/'. $input_method['admin_file'];
  }
  $function = $input_method['module'] .'_galleryapi_'. $preset['input_name'] .'_form';
  if (function_exists($function)) {
    $form['input']['settings']['entries'] = $function($form['input']['settings']['entries']);    
  }
  
  $form['input']['settings']['entries']['slide_types'] = array();
  if (is_array($output_method['slide_types']) && is_array($input_method['formatters'])) {
    foreach ($output_method['slide_types'] as $slide_type) {
      $form['input']['settings']['entries']['slide_types'][$slide_type] = array(
        '#type' => 'select',
        '#title' => t('Select formatter for %type slide type', array('%type' => $slide_type)),
        '#options' => array('' => t('--Select formatter--'), -1 => t('Empty')) + $input_method['formatters'],
        '#default_value' => '',
        '#required' => TRUE,      
      );
    }
  }
  
  _galleryapi_apply_settings($form['input']['settings']['entries'], $preset['input_data']);
  
  $form['output'] = array(
   '#type' => 'fieldset',
   '#title' => t('Output method %output_name settings', array('%output_name' => $output_method['name'])),
  );

  if ($output_method['admin_file']) {
    require_once drupal_get_path('module', $output_method['module']) .'/'. $output_method['admin_file'];
  }
  $function = $output_method['module'] .'_galleryapi_'. $preset['output_name'] .'_form';  
  if (function_exists($function)) {
    $form['output']['settings']['entries'] = $function();
    _galleryapi_apply_settings($form['output']['settings']['entries'], $preset['output_data']);
  } 
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  return $form;
}

function galleryapi_admin_preset_configure_submit($form, &$form_state) {
  switch ($form_state['clicked_button']['#parents'][0]) {
    case 'save':
      $input_data = $form_state['values']['input']['settings']['entries'];
      $output_data = $form_state['values']['output']['settings']['entries']; 
      db_query("UPDATE {galleryapi_presets} SET input_data = '%s', output_data = '%s' WHERE pid = %d",
      serialize($input_data), serialize($output_data), $form_state['pid']);
      drupal_set_message(t('The preset have been updated.'));
      $form_state['redirect'] = 'admin/settings/galleryapi';
      
      // hook_galleryapi_methodapi (operation: preset save)  		
      module_invoke_all('galleryapi_methodapi', 'preset save', $form_state['pid'], $form_state['value']);
    break;
    case 'delete':
      db_query("DELETE FROM {galleryapi_presets} WHERE pid = %d", $form_state['pid']);
      drupal_set_message(t('The preset have been deleted.'));
      $form_state['redirect'] = 'admin/settings/galleryapi';
      
      // hook_galleryapi_methodapi (operation: preset delete)
      module_invoke_all('galleryapi_methodapi', 'preset delete', $form_state['pid']);
    break;
  }
}


function galleryapi_presets_delete($form_state, $preset) {
  $form_state['pid'] = $preset['pid'];
  return confirm_form($form,
                      t('Are you sure you want to delete this preset?'),
                      'admin/settings/galleryapi/', t('This action cannot be undone.'),
                      t('Delete'), t('Cancel'));
}

function galleryapi_presets_delete_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    db_query("DELETE FROM {galleryapi_presets} WHERE pid = %d", $form_state['pid']);
    drupal_set_message(t('The preset have been deleted.'));
  }
  $form_state['redirect'] = 'admin/settings/galleryapi';
  return;
}

function theme_galleryapi_preset_list($presets = array()) {
  $output = '<div class="gallery-table">';
  
  if (!empty($presets)) {
    $inputs = galleryapi_get_input();
    $outputs = galleryapi_get_output();
    
    $header = array(t('Preset'), t('Input method'), t('Output method'), t('Actions'));
    $rows = array();
    
    foreach ($presets as $preset) {
      $row = array();
      $row[] = $preset['name'];
      $row[] = $inputs[$preset['input_name']]['name'];
      $row[] = $outputs[$preset['output_name']]['name'];
      $links = array(
        0 => array(
          'href' => 'admin/settings/galleryapi/'. $preset['pid'] .'/edit',
          'title' => 'edit',
        ),
        1 => array(
          'href' => 'admin/settings/galleryapi/'. $preset['pid'] .'/configure',
          'title' => 'configure',
        ),
        2 => array(
          'href' => 'admin/settings/galleryapi/'. $preset['pid'] .'/delete',
          'title' => 'delete',
        ),
      ); 
      $row[] = theme('links', $links);
      
      $rows[] = $row;     
    }
    
    $output .= theme('table', $header, $rows);    
  }
  
  $output .= '</div>';
  return $output;
}

function galleryapi_get_input_names() { 
  $result = galleryapi_get_input();
  if ($result) {
    $options[''] = t('--Select input method--');
    foreach ( $result as $key => $method) {
      $options[$key] = $method['name']; 
    }
  }
  else {
    $options[''] = t('There are no avialable input methods!');
  }
  return $options;
}

function galleryapi_get_output_names() {
  $result = galleryapi_get_output();
  if ($result) {
    $options[''] = t('--Select output method--');
    foreach ( $result as $key => $method) {
      $options[$key] = $method['name']; 
    }
  }
  else {
    $options[''] = t('There are no avialable output methods!');
  }
  return $options;
}

function _galleryapi_apply_settings(&$form, $data) {
  if (is_array($data)) {
    foreach ($data as $key => $element) {
      if (is_array($element)) {
        _galleryapi_apply_settings($form[$key], $element);
      }
      elseif ($form[$key]['#type']) {
        $form[$key]['#default_value'] = $element;
      }
    }
  }
}